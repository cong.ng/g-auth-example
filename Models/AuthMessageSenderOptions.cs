﻿namespace GoogleAuth.Models
{
    public class AuthMessageSenderOptions
    {
        public string? SendGridKey { get; set; }
    }
}
